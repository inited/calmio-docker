#!/bin/bash -xe

IMAGE_VERSION=`date "+%y%m%d%H%M%S"`

rm -rf build
mkdir build
cd build

for component in admin backend web docker;
do
  git clone git@bitbucket.org:inited/calmio-$component.git
  cd calmio-$component
  git checkout release || true
  cd ..
done

docker run \
  -v "$PWD/calmio-admin":/usr/src/app \
  -w /usr/src/app \
  -e NG_CLI_ANALYTICS='ci' \
  -t \
  -i node:12 \
  /bin/sh -c "export NG_CLI_ANALYTICS='false'; npm install; npm run build --prod"

cd calmio-backend
docker run --rm --interactive --tty --volume $PWD:/app composer install --ignore-platform-reqs --no-scripts
cd ..

cp -rp calmio-admin/dist calmio-web/admin
cp -rp calmio-backend calmio-web/api

cd calmio-web
docker build -t inited/calmio-web:prod -t inited/calmio-web:prod-$IMAGE_VERSION -f Dockerfile-prod .
cd ..

docker login --username pavrda --password d99e8f90-6db0-4b15-a278-e931bf41e2ed
docker push inited/calmio-web:prod
docker push inited/calmio-web:prod-$IMAGE_VERSION
