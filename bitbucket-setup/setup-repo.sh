#!/bin/bash -x

PROJECT=calmio
CURL="curl -n"

if [ ! -f id_rsa -a ! -f id_rsa.pub ]; then
  ssh-keygen -t rsa -b 2048 -f id_rsa -C $PROJECT -N ""
fi

PRIVATE=`cat id_rsa | paste -s -d "\t" | sed "s/\t/\\\\\\\\n/g"`
PUBLIC=`cat id_rsa.pub | tr -d '\n'`

for MODULE in admin api backend docker web; do
  REPO=$PROJECT-$MODULE
  echo $REPO

  # enable pipeline
  $CURL -X PUT -H "Content-type: application/json" -d "{\"enabled\": true,\"type\": \"repository_pipelines_configuration\"}" "https://api.bitbucket.org/2.0/repositories/inited/$REPO/pipelines_config"

  # ssh keys
  $CURL -X PUT -H "content-type: application/json" -d "{\"private_key\":\"$PRIVATE\", \"public_key\":\"$PUBLIC\" }" "https://api.bitbucket.org/2.0/repositories/inited/$REPO/pipelines_config/ssh/key_pair"

  # delete all variables
  VARIABLES=`$CURL -X GET "https://api.bitbucket.org/2.0/repositories/inited/$REPO/pipelines_config/variables/" | json values`
  for VARIABLE in `echo -n $VARIABLES | json -a uuid`; do
    $CURL -X DELETE "https://api.bitbucket.org/2.0/repositories/inited/$REPO/pipelines_config/variables/\{$VARIABLE\}" 
  done

  # repository variables
  UUID=`uuidgen`
  $CURL -X POST -H "Content-type: application/json" -d "{\"type\": \"pipeline_variable\",\"uuid\": \"{$UUID}\",\"key\": \"DOCKER_HUB_USERNAME\",\"value\": \"pavrda\",\"secured\": false}" "https://api.bitbucket.org/2.0/repositories/inited/$REPO/pipelines_config/variables/"
  UUID=`uuidgen`
  $CURL -X POST -H "Content-type: application/json" -d "{\"type\": \"pipeline_variable\",\"uuid\": \"{$UUID}\",\"key\": \"DOCKER_HUB_PASSWORD\",\"value\": \"d99e8f90-6db0-4b15-a278-e931bf41e2ed\",\"secured\": true}" "https://api.bitbucket.org/2.0/repositories/inited/$REPO/pipelines_config/variables/"


  # delete all environment variables
  ENVIRONMENTS=`$CURL -X GET "https://api.bitbucket.org/2.0/repositories/inited/$REPO/environments/" | json values`
  for ENVIRONMENT in `echo -n $ENVIRONMENTS | json -a uuid`; do
    VARIABLES=`$CURL -X GET "https://api.bitbucket.org/2.0/repositories/inited/$REPO/deployments_config/environments/\{$ENVIRONMENT\}/variables" | json values`
    for VARIABLE in `echo -n $VARIABLES | json -a uuid`; do
      $CURL -X DELETE "https://api.bitbucket.org/2.0/repositories/inited/$REPO/deployments_config/environments/\{$ENVIRONMENT\}/variables/\{$VARIABLE\}" 
    done
  done

  # Test environment variables
  ENVIRONMENT_TEST=`$CURL -X GET "https://api.bitbucket.org/2.0/repositories/inited/$REPO/environments/" | json values | json -a uuid -c "this.category.name=='Test'"`

  UUID=`uuidgen`
  $CURL -X POST -H "Content-type: application/json" -d "{\"type\": \"pipeline_variable\",\"uuid\": \"{$UUID}\",\"key\": \"DOCKER_HOST\",\"value\": \"tcp://inited.cz:2375\",\"secured\": false}" "https://api.bitbucket.org/2.0/repositories/inited/$REPO/deployments_config/environments/\{$ENVIRONMENT_TEST\}/variables"


  # Delete all branch rules
  RULES=`curl -n -X GET "https://api.bitbucket.org/2.0/repositories/inited/$REPO/branch-restrictions" | json values`
  for RULE in `echo -n $RULES | json -a id`; do
    $CURL -X DELETE "https://api.bitbucket.org/2.0/repositories/inited/$REPO/branch-restrictions/$RULE"
  done

  # Enforce pull requests
  curl -n -X POST -H "Content-type: application/json" -d '{"kind":"push","branch_match_kind":"glob","pattern":"master","users":[],"groups":[{"owner":{"username":"inited"},"slug":"administrators"}]}' "https://api.bitbucket.org/2.0/repositories/inited/$REPO/branch-restrictions"
  
done

$CURL -X POST -H "Content-type: application/json" -d "{\"label\":\"$PROJECT\",\"key\":\"$PUBLIC\"}" "https://api.bitbucket.org/2.0/repositories/inited/$PROJECT-docker/deploy-keys"


